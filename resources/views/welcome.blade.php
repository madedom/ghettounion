<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="MUSIC, DANCE, GRAFFITI & STREET SPORT. We all live in a ghetto, it's can proved by any city dweller!"/img/>
		<meta name="keywords"  content="гета юнион,гета саюз,гета бай,гето юнион,гетто юнион,гетто союз,ghetto union,geto union,geta union" />
        <title>GHETTO UNION</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #000;
                color: #fff;
                font-family: 'Raleway', sans-serif;
                font-weight: 700;
				font-size:21px;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }
			@media (max-width:768px) {.title {font-size:34px}}

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    GHETTO UNION
                </div>
				We all live in the ghetto, it's can proved by any city dweller!<br><br>
				<span style="font-size: 30px;"><strong>FOLLOW US</strong></span><br />
<a href="https://vk.com/ghettounion" target="_blank" rel="noopener"><img class="alignnone size-full wp-image-11" src="/img/vk.png" alt="" width="64" height="64" /></a><a href="https://facebook.com/ghettounion" target="_blank" rel="noopener"><img class="alignnone size-full wp-image-5" src="/img/facebook.png" alt="" width="64" height="64" /></a><a href="https://instagram.com/ghettounion" target="_blank" rel="noopener"><img class="alignnone size-medium wp-image-7" src="/img/instagram.png" alt="" width="64" height="64" /></a> <a href="https://www.youtube.com/channel/UClftcjmP_ze-sqcEJowF48w" target="_blank" rel="noopener"><img class="alignnone size-medium wp-image-10" src="/img/you-tube.png" alt="" width="64" height="64" /></a> <a href="https://vimeo.com/ghettounion" target="_blank" rel="noopener"><img class="alignnone size-medium wp-image-9" src="/img/vimeo.png" alt="" width="64" height="64" /></a><a href="https://twitter.com/ghettounion" target="_blank" rel="noopener"><img class="alignnone size-medium wp-image-8" src="/img/twitter.png" alt="" width="64" height="64" /></a><a href="https://plus.google.com/b/118133968632142255769/118133968632142255769" target="_blank" rel="noopener"><img class="alignnone size-medium wp-image-6" src="/img/google_plus.png" alt="" width="64" height="64" /></a><br />
				<div style="font-size:12px;padding-top:40px;">The project needs PHP developers and designers.<br> for contact <a style="color:#fff;" href="mailto:mail@ghettounion.com">mail@ghettounion.com</a><br><br>2013-2018</div>
            </div>
			
        </div>
		
    </body>
</html>
